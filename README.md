# nginx-monitor

Auto-reload nginx when config or build server redirects map change

## Installation

* Clone into `/scripts/nginx-monitor`

* `sudo cp nginx-monitor.service /etc/systemd/system/`

* `sudo systemctl enable nginx-monitor.service`

* `sudo systemctl start nginx-monitor.service`

## Watched files/directories

By default, the script will monitor for changes in:

* `/etc/nginx/sites-enabled` - Nginx site configuration files

* `/var/www/build/redirects-map.conf` - The location of our redirect map, that points latest-build URLs to specific versions
